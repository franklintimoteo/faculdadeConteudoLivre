import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class contadorLinhas{
    public static void main(String[] args){
        //Armazenamos o nome do arquivo na variavel novaEntrada
        File novaEntrada = new File("entrada.txt");
        //Para mecher com arquivo, sempre precisamos usar try e catch
        try{
            //Tentamos abrir o abrir e passamos o conteudo para novoLer
            FileReader novoLer      = new FileReader(novaEntrada);
            //FileReader não nos dar muitas opções, dai utilizamos o BufferedReader que só ele 
            //possui a opção readLine()
            BufferedReader texto    = new BufferedReader(novoLer);
            //Pegamos a primeira linha com readLine() e adicionamos a variavel linha
            String linha = texto.readLine();
            //Precisamos de um contador, para isso serve a variavel abaixo
            int numeroDeLinhas = 0;
            //Escolhemos um while, porque enquanto houver linhas ele vai 
            //utilizar um readline e somar numeroDeLinhas a ela mesmo e depois + 1
            while(linha != null){
                numeroDeLinhas = numeroDeLinhas + 1;
                //A variavel linha, obtém mais outra linha, agora a linha 2 (o que na verdade é 1, porque começamos com 0)
                linha = texto.readLine();
            }
        //Caso aconteça algum erro ao abrir o arquivo na linha 09, receberemos um erro
        //esse erro eu imprimo junto com  "Aconteceu um erro: ..."
        }catch(Exception e){
            System.out.println("Aconteceu o erro: "+e);
        }

    }//fim main
}//Fim classe
